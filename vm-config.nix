{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  services.openssh.enable = true;
  services.cron.enable = true;
  virtualisation.virtualbox.guest.enable = true;
  security.sudo.enable = true;

  environment.systemPackages = with pkgs; [
    which
    emacs25-nox
    wget
    python36Full
    python36Packages.virtualenv
    python36Packages.pip
    tmux
    gitAndTools.gitFull
    gcc
    gnumake
    go
    stow
    usbutils
  ];

  services.openssh.extraConfig = "UseDNS no";
  security.sudo.configFile     = "vagrant ALL=(ALL) NOPASSWD: ALL";
  security.initialRootPassword = "vagrant";
  users.extraUsers.vagrant.openssh.authorizedKeys.keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAIEA9jMKLJqb7otuNXmkHZs6YeEC2if1abU8UH7X2CB0Y+Dgz4mLt0wisNQw6NGjfRsgB7O++qQP4BHCWcT69dtzQGt1pR+H9jGrkTrtlcqFIalF+fVZbU2iWjgcIJf4/GtuwYKJGJTqwngIZ5A3e3yuEtRNGFdt6/QaQeOwVbXFSYk= mike@localhost.localdomain"
      ];

}
