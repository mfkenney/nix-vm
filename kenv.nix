with import <nixpkgs> {}; {
     kEnv = stdenv.mkDerivation {
       name = "kmodbuild";
       buildInputs = [ stdenv linux ];
       hardeningDisable = [ "pic" ];
       KDEV="${linux.dev}";
     };
}