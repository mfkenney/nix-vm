let
  pkgs = import <nixpkgs> { };
  prjpkgs = import /src/nixpkgs/default.nix { };
  stdenv = pkgs.stdenv;
in rec {
  armsEnv = stdenv.mkDerivation {
    name = "armsenv";
    buildInputs = [
      stdenv
      pkgs.glib
      pkgs.libuuid
      pkgs.vala
      pkgs.pkgconfig
      pkgs.libtool
      prjpkgs.gs24dsi16wrc-dev
    ];
  };
}
